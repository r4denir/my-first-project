// hello.h
// testing class Hello
// 8 May 2015

#include <iostream>
#include <string>

class Hello {

public:
        Hello(std::string &name) {
                std::cout << "Hello " + name + "!" << std::endl;
        }
};
